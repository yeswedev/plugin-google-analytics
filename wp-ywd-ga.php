<?php
/*
Plugin Name: Simple WP Google Analytics
Description: Add custom UA to scripts, even if you are using multisite configuration
Author: Yes We Dev
Version: 1.0
Author URI: https://yeswedev.bzh
*/

/*
Copyright 2018 Yes We Dev (http://yeswedev.bzh)
*/

add_action('admin_menu', 'my_plugin_menu');
function my_plugin_menu()
{
    add_menu_page('Simple WP Google Analytics', 'Simple WP Google Analytics', 'administrator', 'wp-simple-ga', 'wp_simple_ga_page', 'dashicons-rss');
}

function wp_simple_ga_page()
{
    ?>
    <div class="wrap">
        <h2>Google Analytics details</h2>

        <form method="post" action="options.php">
            <?php settings_fields('google-analytics-ua'); ?>
            <?php do_settings_sections('google-analytics-ua'); ?>
            <table class="form-table">
                <tr valign="top">

                    <td>
                        <label scope="row">Votre UA</label>
                        <input type="text" name="ga_ua" value="<?php echo esc_attr(get_option('ga_ua')); ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="">Anonymisation</label>
                        <input type="checkbox" <?php echo !empty(get_option('ga_anon')) ? 'checked' : ''; ?>
                               name="ga_anon" value="1" <?php checked(1, get_option('ga_anon'), false); ?>>
                    </td>
                </tr>

            </table>

            <?php submit_button(); ?>

        </form>
    </div>
    <?php
}


function wp_simple_ga_settings()
{
    register_setting('google-analytics-ua', 'ga_ua', array(
        'type' => 'string',
        'default' => 'xx-xxxxxxx-xx',
    ));
    register_setting('google-analytics-ua', 'ga_anon', array(
        'type' => 'boolean',
        'default' => false,
    ));
}

add_action('admin_init', 'wp_simple_ga_settings');

function addGAScripts()
{
    $anon = '';
    if (!empty(get_option('ga_anon'))) {
        $anon = '{"anonymizeIp": true}';
    }
    $ua = esc_attr(get_option('ga_ua'));
    echo "<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', ' " . $ua . " ', 'auto');ga('require', 'displayfeatures');ga('send', 'pageview'," . $anon . ");</script>";
}

add_action('wp_head', 'addGAScripts');
